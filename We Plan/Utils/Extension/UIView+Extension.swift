//
//  UIView+Extension.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//
import UIKit

extension UIView {
  
  /// EZSE: add multiple subviews
  public func addSubviews(_ views: [UIView]) {
    views.forEach { [weak self] eachView in
      self?.addSubview(eachView)
    }
  }
  
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
  
}
