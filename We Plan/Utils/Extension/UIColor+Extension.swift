//
//  UIColor+Extension.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//
import UIKit

extension UIColor {
  enum WePaln {
    static let primaryColor = #colorLiteral(red: 0.9137254902, green: 0.2666666667, blue: 0.4156862745, alpha: 1)
  }
}
