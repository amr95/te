//
//  UIHelper.swift
//  We Plan
//
//  Created by apple on 9/24/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit
import DeviceKit
import NVActivityIndicatorView
import SwiftMessages

class UIHelper {
  
  /**
   Returns the height of the Status bar according to the device type.
   - Returns: Status bar height.
   */
  
  class func getStatusBarHeight() -> Int {
    let groupOfAllowedDevices: [Device] = [.iPhoneX,.iPhoneXR,.iPhoneXS,.iPhoneXSMax,.simulator(.iPhoneX),.simulator(.iPhoneXR),.simulator(.iPhoneXS)]
    let device = Device.current
    if device.isOneOf(groupOfAllowedDevices) {
    return 44
    
    } else {
    return 20
    }
  }
  
  /**
   Returns the height of the Tab bar according to the device type.
   - Returns: Tab bar height.
   */
  
  class func getTabBarHeight() -> Int {
    let groupOfAllowedDevices: [Device] = [.iPhoneX,.iPhoneXR,.iPhoneXS,.iPhoneXSMax,.simulator(.iPhoneX),.simulator(.iPhoneXR),.simulator(.iPhoneXS)]
    let device = Device.current
    if device.isOneOf(groupOfAllowedDevices) {
      return 80
    } else {
      return 50
    }
  }

  /**
   Sets the color of the StatusBar.
   - Parameter color: The color to set.
   */
  class func setStatusBarColor(color:UIColor) {
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
      statusBar.backgroundColor = color
    }
  }
  
  /**
   show the activity indicator after loading the data from API.
   */
  class func showActivityIndicator() {
    let activityData = ActivityData(size: nil, message: nil, messageFont: nil, messageSpacing: nil, type: .ballSpinFadeLoader , color: UIColor.WePaln.primaryColor, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil)
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
  }
  
  /**
   Hide the activity indicator after loading the data from API.
   */
  class func hideActivityIndicator() {
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
  }
  
  /**
   Shows a snack bar with a specific message.
   - Parameter message: The message to display.
   - Parameter showRetryButton: A boolean to show retry button or not.
   - Parameter completion: Completion handler.
   */
  class func showSnackBar(message:String, showRetryButton:Bool, completion:(() -> Void)? = nil) {
    let view = MessageView.viewFromNib(layout: .messageView)
    view.configureTheme(.info)
    view.configureDropShadow()
    view.backgroundColor = UIColor.white
    view.configureContent(title: "", body: message)
    view.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//    (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
    if showRetryButton {
      view.button?.isHidden = false
      view.button?.backgroundColor = UIColor.darkGray
      view.button?.setTitle("retry".localized(), for: .normal)
//      view.button?.addTapGesture(action: { (_) in
//        if Utils.isInternetAvailable() {
//          SwiftMessages.hide()
//          completion!()
//          autoHideSnackbar = true
//        }
//        
//      })
    } else {
      view.button?.isHidden = true
    }
    
    SwiftMessages.defaultConfig.duration = .forever
    SwiftMessages.defaultConfig.presentationStyle = .bottom
    SwiftMessages.show(view: view)
  }
  
  /**
   Hides the snack bar.
   */
  class func hideSnackBar() {
    SwiftMessages.hide()
  }
}
