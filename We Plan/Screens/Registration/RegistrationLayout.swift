//
//  RegistrationLayout.swift
//  We Plan
//
//  Created by Amr Saleh on 10/5/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import SnapKit
import DropDown

protocol RegistrationLayoutDelegate: class {
    func selectCategoryButtonTapped()
}


class RegistrationLayout {
    var superview: UIView!
    weak var delegate:RegistrationLayoutDelegate?
    
    lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var Label1:UILabel = {
        let label = UILabel()
        label.text = "Create an Account"
        label.font = AppFont.getFont(type: .noramlBold, size: 28)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var FullNameLabel:UILabel = {
        let label = UILabel()
        label.text = "Full Name"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var FullNameContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var FullNameTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "Full Name"
        textField.font = AppFont.getFont(type: .normalRegular, size: 13)
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        textField.tintColor = UIColor.WePaln.primaryColor
        return textField
    }()
    
    lazy var EmailLabel:UILabel = {
        let label = UILabel()
        label.text = "Email"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var EmailContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var EmailTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "Email"
        textField.font = AppFont.getFont(type: .normalRegular, size: 13)
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        textField.tintColor = UIColor.WePaln.primaryColor
        return textField
    }()
    
    
    lazy var PasswordLabel:UILabel = {
        let label = UILabel()
        label.text = "Password"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var PasswordContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var PasswordTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "●●●●●●●●●●"
        textField.font = AppFont.getFont(type: .normalRegular, size: 13)
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        textField.isSecureTextEntry = true
        textField.tintColor = UIColor.WePaln.primaryColor
        return textField
    }()
    
    
    lazy var PhoneLabel:UILabel = {
        let label = UILabel()
        label.text = "Phone"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var PhoneContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var PhoneTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "01000000000"
        textField.font = AppFont.getFont(type: .normalRegular, size: 13)
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        textField.tintColor = UIColor.WePaln.primaryColor
        return textField
    }()
    
    
    lazy var CategoryLabel:UILabel = {
        let label = UILabel()
        label.text = "Category"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var CategoryContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var selectCategoryButton:UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        button.setTitle("Select Your Category", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = AppFont.getFont(type: .normalRegular, size: 13)
        button.addTapGesture(action: { [weak self] (_) in
            self?.delegate?.selectCategoryButtonTapped()
        })
        return button
    }()
    
    lazy var ToolsLabel:UILabel = {
        let label = UILabel()
        label.text = "Tools"
        label.font = AppFont.getFont(type: .normalRegular, size: 14)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var ToolsContainerView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var Tools1Button:UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setBackgroundImage(UIImage(named: "1"), for: .normal)
        button.addTapGesture(action: { [weak self] (_) in
        })
        return button
    }()
    
    lazy var Tools2Button:UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setBackgroundImage(UIImage(named: "2"), for: .normal)
        button.addTapGesture(action: { [weak self] (_) in
        })
        return button
    }()
    
    lazy var Tools3Button:UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setBackgroundImage(UIImage(named: "3"), for: .normal)
        button.addTapGesture(action: { [weak self] (_) in
        })
        return button
    }()
    
    init(superview: UIView, delegate:RegistrationLayoutDelegate) {
        self.superview = superview
        self.delegate = delegate
    }
    
    func setupViews() {
        superview.addSubviews([logoImageView,Label1,FullNameLabel,FullNameContainerView,FullNameTextField, EmailLabel, EmailContainerView, EmailTextField, PasswordLabel, PasswordContainerView, PasswordTextField, PhoneLabel, PhoneContainerView, PhoneTextField, CategoryLabel, CategoryContainerView, selectCategoryButton, ToolsLabel, ToolsContainerView, Tools1Button, Tools2Button, Tools3Button])
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(44)
            make.centerX.equalToSuperview()
            make.height.equalTo(97.28)
        }
        
        Label1.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(logoImageView.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
        
        FullNameLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(Label1.snp.bottom).offset(19)
        }
        FullNameContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(50)
            make.top.equalTo(FullNameLabel.snp.bottom).offset(3.53)
        }
        FullNameTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(FullNameContainerView.snp.leading).offset(22)
            make.trailing.equalTo(FullNameContainerView.snp.trailing).offset(-22)
            make.top.equalTo(FullNameContainerView.snp.top).offset(15)
        }
        
        EmailLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(FullNameContainerView.snp.bottom).offset(1)
        }
        EmailContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(50)
            make.top.equalTo(EmailLabel.snp.bottom).offset(3.53)
        }
        EmailTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(EmailContainerView.snp.leading).offset(22)
            make.trailing.equalTo(EmailContainerView.snp.trailing).offset(-22)
            make.top.equalTo(EmailContainerView.snp.top).offset(15)
        }
        
        PasswordLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(EmailContainerView.snp.bottom).offset(1)
        }
        PasswordContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(50)
            make.top.equalTo(PasswordLabel.snp.bottom).offset(3.53)
        }
        PasswordTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(PasswordContainerView.snp.leading).offset(22)
            make.trailing.equalTo(PasswordContainerView.snp.trailing).offset(-22)
            make.top.equalTo(PasswordContainerView.snp.top).offset(15)
        }
        
        PhoneLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(PasswordContainerView.snp.bottom).offset(1)
        }
        PhoneContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(50)
            make.top.equalTo(PhoneLabel.snp.bottom).offset(3.53)
        }
        PhoneTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(PhoneContainerView.snp.leading).offset(22)
            make.trailing.equalTo(PhoneContainerView.snp.trailing).offset(-22)
            make.top.equalTo(PhoneContainerView.snp.top).offset(15)
        }
        
        CategoryLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(PhoneContainerView.snp.bottom).offset(1)
        }
        CategoryContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(50)
            make.top.equalTo(CategoryLabel.snp.bottom).offset(3.53)
        }
        selectCategoryButton.snp.makeConstraints { (make) in
            make.leading.equalTo(CategoryContainerView.snp.leading).offset(22)
            make.trailing.equalTo(CategoryContainerView.snp.trailing).offset(-22)
            make.top.equalTo(CategoryContainerView.snp.top).offset(13)
        }
        
        ToolsLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.top.equalTo(CategoryContainerView.snp.bottom).offset(1)
        }
        ToolsContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(95)
            make.top.equalTo(ToolsLabel.snp.bottom).offset(3.53)
        }
        
        Tools1Button.snp.makeConstraints { (make) in
            make.leading.equalTo(ToolsContainerView.snp.leading).offset(10)
            make.width.equalTo(95)
            make.height.equalTo(95)
            make.top.equalTo(ToolsContainerView.snp.top)
        }
        
        Tools2Button.snp.makeConstraints { (make) in
            make.leading.equalTo(Tools1Button.snp.trailing).offset(3)
            make.width.equalTo(95)
            make.height.equalTo(95)
            make.top.equalTo(ToolsContainerView.snp.top)
        }
        Tools3Button.snp.makeConstraints { (make) in
            make.leading.equalTo(Tools2Button.snp.trailing).offset(4)
            make.width.equalTo(95)
            make.height.equalTo(95)
            make.top.equalTo(ToolsContainerView.snp.top)
        }
    }
}
