//
//  RegisterVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/5/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import DropDown

class RegisterVC: UIViewController {

    var registrationLayout: RegistrationLayout?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        registrationLayout = RegistrationLayout(superview: self.view, delegate: self)
        registrationLayout?.setupViews()
        // Do any additional setup after loading the view.
    }
    


}

extension RegisterVC: RegistrationLayoutDelegate {
    func selectCategoryButtonTapped() {
        let dropDown = DropDown()
        // The view to which the drop down will appear on
        dropDown.anchorView = registrationLayout?.selectCategoryButton // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Makeup Artist","Photographer"]

        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.registrationLayout?.selectCategoryButton.setTitle(item, for: .normal)
        }
    }


}
