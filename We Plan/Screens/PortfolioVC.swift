//
//  PortfolioVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/11/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import Segmentio

    class PortfolioVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var buttonTabs: Segmentio!
        @IBOutlet weak var collectionView: UICollectionView!
        
    var selectedIndex = 0
    var content = [SegmentioItem]()
    
    let tornadoItem = SegmentioItem(
        title: "Portfolio",
        image: nil)
    
    let tornadoItem2 = SegmentioItem(
        title: "Reviews",
        image: nil)
    
    let tornadoItem3 = SegmentioItem(
        title: "Packges",
        image: nil)
    
    var pImages = ["x1","x2","x3","x4","x5" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCellNib(cellClass: AlbumCVC.self)
        collectionView.registerCellNib(cellClass: ReviewsCVC.self)
        collectionView.registerCellNib(cellClass: PackagesCVC.self)

        
        content.append(tornadoItem)
        content.append(tornadoItem2)
        content.append(tornadoItem3)


        // Do any additional setup after loading the view.
        buttonTabs.setup(
            content: content,
            style: SegmentioStyle.onlyLabel,
            options: nil
        )
    }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
            buttonTabs.valueDidChange = { segmentio, segmentIndex in
                print("Selected item: ", segmentIndex)
                self.selectedIndex = segmentIndex
                self.collectionView.reloadData()
            }
        }
    
//    buttonTabs.valueDidChange = { segmentio, segmentIndex in
//    print("Selected item: ", segmentIndex)
//    }
    override func viewDidLayoutSubviews() {
        profileImage.layer.cornerRadius = 50
        curvedView.layer.cornerRadius = 80
        
    }

}

extension PortfolioVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pImages.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedIndex == 0 {
            let cell = collectionView.dequeue(indexPath: indexPath) as AlbumCVC
            if indexPath.row == 0 {
                cell.albumImage.contentMode = .center
                cell.albumImage.image = UIImage(named: "plus")
            } else {
                cell.albumImage.image = UIImage(named: pImages[(indexPath.row) - 1])
            }
            return cell
        } else if selectedIndex == 1 {
            let cell = collectionView.dequeue(indexPath: indexPath) as ReviewsCVC
            return cell
        } else {
            let cell = collectionView.dequeue(indexPath: indexPath) as PackagesCVC
            return cell
        }
    }
    
}

extension PortfolioVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if selectedIndex == 0 {
            return CGSize(width: 150, height: 150)
        } else if selectedIndex == 1 {
            return CGSize(width: UIScreen.main.bounds.width, height: 130.0)
        } else {
            return CGSize(width: UIScreen.main.bounds.width, height: 80.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if selectedIndex == 0 {
            return UIEdgeInsets(top: 15.0, left: 30.0, bottom: 0.0, right: 30.0)
        } else {
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
    }
    
}
