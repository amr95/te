//
//  LoginVC.swift
//  We Plan
//
//  Created by apple on 9/24/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import SnapKit

class LoginVCSnapKit: UIViewController {

  lazy var logoImageView:UIImageView = {
    let imageView = UIImageView()
    imageView.image = UIImage(named: "logo")
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  lazy var FBButton:UIButton = {
    let fbButton = UIButton()
    fbButton.setTitle("Login with Facebook", for: .normal)
    fbButton.backgroundColor = #colorLiteral(red: 0.2352941176, green: 0.3137254902, blue: 0.5019607843, alpha: 1)
    fbButton.titleLabel?.font = AppFont.getFont(type: .normalRegular, size: 12)
    fbButton.layer.cornerRadius = 24
 //   fbButton.setImage(UIImage(named: "facebook2"), for: .normal)
    return fbButton
  }()
  
  lazy var orLabel:UILabel = {
    let label = UILabel()
    label.text = "or"
    label.font = AppFont.getFont(type: .normalRegular, size: 12)
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 0.4509803922, green: 0.4509803922, blue: 0.4509803922, alpha: 1)
    return label
  }()
  
  lazy var emailContainerView:UIView = {
    var view = UIView()
    view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
    view.layer.cornerRadius = 8
    return view
  }()
  
  lazy var emailTextField:UITextField = {
    let textField = UITextField()
    textField.placeholder = "Enter your email address"
    textField.font = AppFont.getFont(type: .normalRegular, size: 12)
    textField.spellCheckingType = .no
    textField.autocorrectionType = .no
    textField.tintColor = UIColor.WePaln.primaryColor
    return textField
  }()
  
  lazy var passwordContainerView:UIView = {
    var view = UIView()
    view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
    view.layer.cornerRadius = 8
    return view
  }()
  
  lazy var passwordTextField:UITextField = {
    let textField = UITextField()
    textField.placeholder = "Password"
    textField.font = AppFont.getFont(type: .normalRegular, size: 12)
    textField.spellCheckingType = .no
    textField.autocorrectionType = .no
    textField.isSecureTextEntry = true
    textField.tintColor = UIColor.WePaln.primaryColor
    return textField
  }()
  
  lazy var loginButton:UIButton = {
    let button = UIButton()
    button.setTitle("Sign In", for: .normal)
    button.backgroundColor = UIColor.WePaln.primaryColor
    button.titleLabel?.font = AppFont.getFont(type: .noramlBold, size: 12)
    button.layer.cornerRadius = 8
    return button
  }()
  
  lazy var forgetPasswordButton:UIButton = {
    let button = UIButton()
    button.setTitle("Forgot Password?", for: .normal)
    button.setTitleColor(UIColor.black, for: .normal)
    button.backgroundColor = UIColor.clear
    button.titleLabel?.font = AppFont.getFont(type: .normalRegular, size: 12)
    return button
  }()
  
  lazy var regLabel:UILabel = {
    let label = UILabel()
    label.text = "Don't have an account? "
    label.font = AppFont.getFont(type: .normalRegular, size: 12)
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1)
    return label
  }()
  
  lazy var joinButton:UIButton = {
    let button = UIButton()
    button.setTitle("Join", for: .normal)
    button.setTitleColor(UIColor.black, for: .normal)
    button.backgroundColor = UIColor.clear
    button.titleLabel?.font = AppFont.getFont(type: .normalRegular, size: 12)
    return button
  }()
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      view.backgroundColor = UIColor.white
      setupViews()
    }
  
  func setupViews() {
    
    view.addSubviews([logoImageView,FBButton,orLabel,emailContainerView,emailTextField,passwordContainerView,passwordTextField,loginButton,forgetPasswordButton,regLabel,joinButton])
    
    logoImageView.snp.makeConstraints { (make) in
      make.width.height.equalTo(133.6)
      make.top.equalTo(UIScreen.main.bounds.height * 0.11)
      make.leading.equalTo(UIScreen.main.bounds.width * 0.33)
    }
    
    FBButton.snp.makeConstraints { (make) in
      make.top.equalTo(logoImageView.snp.bottom).offset(52.9)
      make.leading.equalToSuperview().offset(34)
      make.height.equalTo(48)
      make.width.equalTo(UIScreen.main.bounds.width * 0.82)
    }
    
    orLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(UIScreen.main.bounds.width * 0.25)
      make.height.equalTo(18)
      make.width.equalTo(UIScreen.main.bounds.width * 0.484)
      make.top.equalTo(FBButton.snp.bottom).offset(10)
    }
    
    emailContainerView.snp.makeConstraints { (make) in
      make.top.equalTo(orLabel.snp.bottom).offset(30)
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(40)
    }
    
    emailTextField.snp.makeConstraints { (make) in
      make.top.equalTo(emailContainerView.snp.top).offset(12)
      make.leading.equalTo(emailContainerView.snp.leading).offset(35.9)
      make.width.equalTo(297)
    }
    
    passwordContainerView.snp.makeConstraints { (make) in
      make.top.equalTo(emailContainerView.snp.bottom).offset(30)
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(40)
    }
    
    passwordTextField.snp.makeConstraints { (make) in
      make.top.equalTo(passwordContainerView.snp.top).offset(12)
      make.leading.equalTo(passwordContainerView.snp.leading).offset(35.9)
      make.width.equalTo(297)
    }
    
    loginButton.snp.makeConstraints { (make) in
      make.top.equalTo(passwordContainerView.snp.bottom).offset(30)
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(40)
    }
    
    forgetPasswordButton.snp.makeConstraints { (make) in
      make.top.equalTo(loginButton.snp.bottom).offset(13)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(18)
    }
    
    regLabel.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(86)
      make.top.equalTo(loginButton.snp.bottom).offset(81)
    }
    
    joinButton.snp.makeConstraints { (make) in
      make.leading.equalTo(regLabel.snp.trailing)
      make.top.equalTo(regLabel.snp.top).offset(-7)
    }
  }

}
