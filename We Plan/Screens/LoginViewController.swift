//
//  LoginViewController.swift
//  We Plan
//
//  Created by Amr Saleh on 10/9/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var FBButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        FBButton.layer.cornerRadius = 8
        loginButton.layer.cornerRadius = 8
        loginButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
    

}
