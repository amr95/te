//
//  getStart.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import SnapKit

protocol getStartVCDelegate: class {
  func getStartButtonTapped()
}

class GetStart: UIViewController, getStartVCDelegate {
  func getStartButtonTapped() {
    present(VCBuilder.homeVC(), animated: true, completion: nil)
  }
  
  weak var delegate:getStartVCDelegate?

  lazy var mainImageView:UIImageView = {
    let imageView = UIImageView()
    imageView.image = UIImage(named: "home")
    imageView.contentMode = .scaleAspectFill
    return imageView
  }()
  
  lazy var blackOverlayImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = UIImage(named: "blackOverlay")
    imageView.contentMode = .scaleAspectFill
    return imageView
  }()
  
  lazy var logoImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = UIImage(named: "logo")
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  lazy var firstLabel:UILabel = {
    let label = UILabel()
    label.text = "We Plan"
    label.font = AppFont.getFont(type: .noramlBold, size: 34)
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    return label
  }()
  
  lazy var secondLabel:UILabel = {
    let label = UILabel()
    label.text = "your greatest moments"
    label.font = AppFont.getFont(type: .normalRegular, size: 22)
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    return label
  }()
  
  lazy var pageControl:UIPageControl = {
    let pageControl = UIPageControl()
    pageControl.numberOfPages = 3
    pageControl.currentPageIndicatorTintColor = UIColor.WePaln.primaryColor
    pageControl.pageIndicatorTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    pageControl.currentPage = 0
    return pageControl
  }()
  
  lazy var getStartedButton:UIButton = {
    let button = UIButton()
  //  button.backgroundColor = UIColor.WePaln.primaryColor
    button.setTitle("Get Started", for: .normal)
    button.titleLabel?.font = AppFont.getFont(type: .noramlBold, size: 12)
    button.setBackgroundImage(UIImage(named: "button"), for: .normal)
    button.addTapGesture(action: { [weak self] (_) in
      self?.delegate?.getStartButtonTapped()
    })
    return button
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }
  
  
  func setupViews() {
    view.addSubviews([mainImageView,blackOverlayImageView,logoImageView,firstLabel,secondLabel,getStartedButton,pageControl])
    
    mainImageView.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.leading.trailing.equalToSuperview()
      make.height.equalTo(UIScreen.main.bounds.height * 0.75)
    }
    blackOverlayImageView.snp.makeConstraints { (make) in
      make.leading.trailing.bottom.equalToSuperview()
      make.height.equalTo(UIScreen.main.bounds.height * 0.85)
    }
    logoImageView.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(116)
      make.width.equalTo(144)
      make.height.equalTo(137)
      make.top.equalTo(UIScreen.main.bounds.height * 0.46)
    }
    firstLabel.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(35)
      make.width.equalTo(UIScreen.main.bounds.width * 0.82)
      make.top.equalTo(logoImageView.snp.bottom).offset(48)
    }
    secondLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(firstLabel.snp.leading)
      make.width.equalTo(firstLabel.snp.width)
      make.top.equalTo(firstLabel.snp.bottom)
    }
    pageControl.snp.makeConstraints { (make) in
      make.centerX.equalToSuperview()
   //   make.centerY.equalTo(UIScreen.main.bounds.height * 0.81)
      make.bottom.equalTo(getStartedButton.snp.top).offset(-12)
    }
    getStartedButton.snp.makeConstraints { (make) in
      make.leading.equalTo(firstLabel.snp.leading)
      make.bottom.equalToSuperview().offset(-44)
      make.width.equalTo(firstLabel.snp.width)
      make.height.equalTo(44)
    }
  }
}

