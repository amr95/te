//
//  SelectTittleVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class SelectTittleVC: UIViewController {

    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
    }
    
    override func viewDidLayoutSubviews() {
        nextButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
        nextButton.layer.cornerRadius = 12

    }

}
