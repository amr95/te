//
//  RegVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/9/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class RegVC: UIViewController {

    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var tittleTextField: UITextField!
   
    let tittles = ["Makeup Artist","Photographer"]
    var selectedTittle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Registration"
        continueButton.layer.cornerRadius = continueButton.frame.height / 2
        continueButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
        createTittlePicker()
        createToolBar()
    }
    
    func createTittlePicker() {
        let tittlePicker = UIPickerView()
        tittlePicker.dataSource = self
        tittlePicker.delegate = self
        tittleTextField.inputView = tittlePicker
    }
    
    func createToolBar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(RegVC.dismissKeyboard))
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        tittleTextField.inputAccessoryView = toolBar
    }
    
    override func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func continueButtonTapped(_ sender: Any) {
    }
    

    
}

extension RegVC: UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.tittles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.tittles[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedTittle = self.tittles[row]
        tittleTextField.text = selectedTittle
    }
}
