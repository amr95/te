//
//  LoginPresenter.swift
//  We Plan
//
//  Created by Amr Saleh on 10/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol LoginViewDelegate {
    func displayLoading()
    func dismissLoading()
    func displayError()
    func displayError(error: String)
}

class LoginPresenter {
    
    var delegate: LoginViewDelegate?
    
    // attatch view to presenter
    func attatchView(view: LoginViewDelegate){
        delegate = view
    }
    // detach view to presenter
    func detachView(){
        delegate = nil
    }
    
    func startNetworking(email: String , password: String) {
        self.delegate?.displayLoading()
        let url = "http://We-plan.net/api/customer/auth/login"
        
        let params = ["email": email ,
                      "password": password  ]
        
        let headers = [
            "lang": "en",
            "category_id": "1"
        ]
        
        APIHelper.fetchGenericsData(stringURL: url, method: .post, parameters: params, headers: headers) { (loginData: LoginModel) in
            let data = loginData.data as! DataClass
            print(data.email)
        }
    }
    
    
    
    
}
