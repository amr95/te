//
//  LoginVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var facebookLogin: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var joinButton: UIButton!
    
    let presenter = LoginPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.attatchView(view: self)
    }
    
    @IBAction func fbButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        presenter.startNetworking(email: email, password: password)
    }
    
    @IBAction func forgetPasswordTapped(_ sender: Any) {
    }
    @IBAction func joinButtonTapped(_ sender: Any) {
    }
}

extension LoginVC: LoginViewDelegate {
    func displayLoading() {
        
    }
    
    func dismissLoading() {
        
    }
    
    func displayError() {
        
    }
    
    func displayError(error: String) {
        
    }
    
    
}
