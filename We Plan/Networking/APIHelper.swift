//
//  APIHelper.swift
//  We Plan
//
//  Created by Amr Saleh on 10/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import Alamofire

class APIHelper {
    
    class func fetchGenericsData<T: Decodable>(stringURL:String ,method: HTTPMethod, parameters: [String:Any]?, headers: HTTPHeaders? , completionHandler: @escaping (T) -> ()) {
        print("URL = \(stringURL)")
        
        
        Alamofire.request(stringURL, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: headers)
            .responseJSON { (resposneData) in
                let data = resposneData
                print(data)
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data.data!)
                    print("obj = \(obj)")
                    completionHandler(obj)
                } catch {
                    print("ERROR")
                }
        }
    }
    
}
